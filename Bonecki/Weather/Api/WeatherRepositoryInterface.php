<?php


namespace Bonecki\Weather\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface WeatherRepositoryInterface
{

    /**
     * Save Weather
     * @param \Bonecki\Weather\Api\Data\WeatherInterface $weather
     * @return \Bonecki\Weather\Api\Data\WeatherInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Bonecki\Weather\Api\Data\WeatherInterface $weather
    );

    /**
     * Retrieve Weather
     * @param string $weatherId
     * @return \Bonecki\Weather\Api\Data\WeatherInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($weatherId);

    /**
     * Retrieve Weather matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Bonecki\Weather\Api\Data\WeatherSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Weather
     * @param \Bonecki\Weather\Api\Data\WeatherInterface $weather
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Bonecki\Weather\Api\Data\WeatherInterface $weather
    );

    /**
     * Delete Weather by ID
     * @param string $weatherId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($weatherId);
}
