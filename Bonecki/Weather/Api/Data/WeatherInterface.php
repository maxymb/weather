<?php


namespace Bonecki\Weather\Api\Data;

interface WeatherInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const TEMPERATURE = 'temperature';
    const WEATHER_ID = 'weather_id';
    const CREATED_AT = 'created_at';

    /**
     * Get weather_id
     * @return string|null
     */
    public function getWeatherId();

    /**
     * Set weather_id
     * @param string $weatherId
     * @return \Bonecki\Weather\Api\Data\WeatherInterface
     */
    public function setWeatherId($weatherId);

    /**
     * Get temperature
     * @return string|null
     */
    public function getTemperature();

    /**
     * Set temperature
     * @param string $temperature
     * @return \Bonecki\Weather\Api\Data\WeatherInterface
     */
    public function setTemperature($temperature);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Bonecki\Weather\Api\Data\WeatherExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Bonecki\Weather\Api\Data\WeatherExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Bonecki\Weather\Api\Data\WeatherExtensionInterface $extensionAttributes
    );

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     * @param string $createdAt
     * @return \Bonecki\Weather\Api\Data\WeatherInterface
     */
    public function setCreatedAt($createdAt);
}
