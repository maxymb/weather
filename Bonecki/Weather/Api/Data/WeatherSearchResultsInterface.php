<?php


namespace Bonecki\Weather\Api\Data;

interface WeatherSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Weather list.
     * @return \Bonecki\Weather\Api\Data\WeatherInterface[]
     */
    public function getItems();

    /**
     * Set temperature list.
     * @param \Bonecki\Weather\Api\Data\WeatherInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
