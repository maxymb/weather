<?php


namespace Bonecki\Weather\Model;

use Magento\Framework\Api\DataObjectHelper;
use Bonecki\Weather\Api\Data\WeatherInterface;
use Bonecki\Weather\Api\Data\WeatherInterfaceFactory;

class Weather extends \Magento\Framework\Model\AbstractModel
{

    protected $weatherDataFactory;

    protected $_eventPrefix = 'bonecki_weather_weather';
    protected $dataObjectHelper;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param WeatherInterfaceFactory $weatherDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Bonecki\Weather\Model\ResourceModel\Weather $resource
     * @param \Bonecki\Weather\Model\ResourceModel\Weather\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        WeatherInterfaceFactory $weatherDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Bonecki\Weather\Model\ResourceModel\Weather $resource,
        \Bonecki\Weather\Model\ResourceModel\Weather\Collection $resourceCollection,
        array $data = []
    ) {
        $this->weatherDataFactory = $weatherDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve weather model with weather data
     * @return WeatherInterface
     */
    public function getDataModel()
    {
        $weatherData = $this->getData();
        
        $weatherDataObject = $this->weatherDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $weatherDataObject,
            $weatherData,
            WeatherInterface::class
        );
        
        return $weatherDataObject;
    }
}
