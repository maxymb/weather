<?php


namespace Bonecki\Weather\Model\ResourceModel;

class Weather extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('bonecki_weather_weather', 'weather_id');
    }
}
