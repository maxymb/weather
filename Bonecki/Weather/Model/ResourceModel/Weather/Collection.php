<?php


namespace Bonecki\Weather\Model\ResourceModel\Weather;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Bonecki\Weather\Model\Weather::class,
            \Bonecki\Weather\Model\ResourceModel\Weather::class
        );
    }
}
