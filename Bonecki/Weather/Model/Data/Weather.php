<?php


namespace Bonecki\Weather\Model\Data;

use Bonecki\Weather\Api\Data\WeatherInterface;

class Weather extends \Magento\Framework\Api\AbstractExtensibleObject implements WeatherInterface
{

    /**
     * Get weather_id
     * @return string|null
     */
    public function getWeatherId()
    {
        return $this->_get(self::WEATHER_ID);
    }

    /**
     * Set weather_id
     * @param string $weatherId
     * @return \Bonecki\Weather\Api\Data\WeatherInterface
     */
    public function setWeatherId($weatherId)
    {
        return $this->setData(self::WEATHER_ID, $weatherId);
    }

    /**
     * Get temperature
     * @return string|null
     */
    public function getTemperature()
    {
        return $this->_get(self::TEMPERATURE);
    }

    /**
     * Set temperature
     * @param string $temperature
     * @return \Bonecki\Weather\Api\Data\WeatherInterface
     */
    public function setTemperature($temperature)
    {
        return $this->setData(self::TEMPERATURE, $temperature);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Bonecki\Weather\Api\Data\WeatherExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Bonecki\Weather\Api\Data\WeatherExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Bonecki\Weather\Api\Data\WeatherExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->_get(self::CREATED_AT);
    }

    /**
     * Set created_at
     * @param string $createdAt
     * @return \Bonecki\Weather\Api\Data\WeatherInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }
}
