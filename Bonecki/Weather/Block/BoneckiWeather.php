<?php


namespace Bonecki\Weather\Block;


class BoneckiWeather extends \Magento\Framework\View\Element\Template
{


    protected $weatherFactory;

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Bonecki\Weather\Model\WeatherFactory $weatherFactory ,
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Bonecki\Weather\Model\WeatherFactory $weatherFactory,
        array $data = []
    )
    {
        $this->weatherFactory = $weatherFactory;
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getWeather()
    {
        $weathers = $this->weatherFactory->create();
        $collection = $weathers->getCollection();
        return  $collection->getLastItem()->getData('temperature');
    }
}
