<?php


namespace Bonecki\Weather\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $table_bonecki_weather_weather = $setup->getConnection()->newTable($setup->getTable('bonecki_weather_weather'));

        $table_bonecki_weather_weather->addColumn(
            'weather_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true,'nullable' => false,'primary' => true,'unsigned' => true,],
            'Entity ID'
        );

        $table_bonecki_weather_weather->addColumn(
            'temperature',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'temperature'
        );

        $table_bonecki_weather_weather->addColumn(
            'created_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            [],
            'created_at'
        );

        //Your install script

        $setup->getConnection()->createTable($table_bonecki_weather_weather);
    }
}
