<?php


namespace Bonecki\Weather\Cron;

use Bonecki\Weather\Api\WeatherRepositoryInterface;

class GetWeather
{
    /**
     * @var  \Psr\Log\LoggerInterface
     */
    protected $logger;
    /**
     * @var \Magento\Framework\HTTP\Adapter\Curl
     */
    protected $curl;
    /**
     * @var \Bonecki\Weather\Api\WeatherRepositoryInterface
     */
    protected $weatherRepository;
    /**
     * @var \Bonecki\Weather\Model\WeatherFactory
     */
    protected $weatherFactory;
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $datetime;

    /**
     * Constructor
     *
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\HTTP\Adapter\Curl $curl
     * @param \Bonecki\Weather\Api\WeatherRepositoryInterface $weatherRepository
     * @param \Bonecki\Weather\Model\WeatherFactory $weatherFactory
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $datetime
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\HTTP\Adapter\Curl $curl,
        \Bonecki\Weather\Api\WeatherRepositoryInterface $weatherRepository,
        \Bonecki\Weather\Model\WeatherFactory $weatherFactory,
        \Magento\Framework\Stdlib\DateTime\DateTime $datetime
    )
    {
        $this->logger = $logger;
        $this->curl = $curl;
        $this->weatherRepository = $weatherRepository;
        $this->weatherFactory = $weatherFactory;
        $this->datetime = $datetime;
    }

    /**
     * Execute the cron
     *
     * @return void
     */
    public function execute()
    {
        $this->logger->addInfo('Cronjob getWeather executed');

        $weatherJson = $this->getLublinWeather();
        $weather = $this->weatherFactory->create();
        $weather->setTemperature($weatherJson);
        $weather->setCreatedAt($this->datetime->date());
        $weather->save();
    }

    protected function getLublinWeather()
    {
        $this->curl->write('GET', 'https://api.openweathermap.org/data/2.5/weather?q=Lublin,pl&appid=1b34b4ade3a75fd6cfe4da266fc4871f');
        $response = $this->curl->read();
        $responseArray = explode("\n", $response);
        $body = array_slice($responseArray, -1);
        $json = json_decode($body[0]);
        $weather = json_decode(json_encode($json->weather[0]), true);

        return $weather['description'];
    }

}